/**Copyright [2011-2014] [University of Siegen, Embedded System Instiute]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 * TTE-patch Receiving native class Implementation 
 * @author <a href="mailto:hamzah.dakheel@uni-siegen.de">Hamzah Dakheel</a> 
 * @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
 *		�2012
 */
  
/* 
 *
 *JniListener.c is an Jni native class which is responsible for opening
 *listening channel for each node in order to receive TT messages,
 *then this class will be reposible for queueing the mreceived messages,
 *processing them and dilevering them to the associated java class.
 
 *comm.h file is a TTTech library which is reposible for sending and receiving 
 *the frames within TTE network by using raw socket technique.
 *in order to uncomment this library and its related function and reuse it again
 *you should have the licenses from TTTEch.
 *
 */
#include <jni.h>
#include "org_universaal_ttelistener_TTEMsgFetching.h" 
#include "org_universaal_ttelistener_TTEMsgListening.h"


//#include "comm.h" 
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <net/ethernet.h>
#include <unistd.h>
#include<signal.h>

//Here, in this code only two listening channels have been created.
#define DEV_RX1       "tt101"
#define DEV_RX3       "tt103"
#define ETH_RX_PROTO  ETH_P_ALL

#define Max (200) 
 char *queue[Max];
 static char* MyId;
 int in_index=0;
 int out_index=0;

    int sock_rx;
static char stMsg[4000]="";


void sighandler (int sig){
  printf("the Signal %d is caught\n",sig);
  //comm_close(sock_rx);
  
}


//  inserting the received messages ina FIFO queue.
int insert (char *str){
if ((in_index+1)% Max == out_index) return 0;
queue[in_index]=str;
in_index = (in_index+1)%Max;
return 1;

}
//  removing one message from the queue to be dilevered then to the related java class
char* remov (){
char *temp;
if (in_index== out_index) return NULL;
temp = queue[out_index];
out_index = (out_index+1)%Max;
return temp;

}

// the listener thread for node 1.
void receiveTT101 ()
{
//recieving messages through TTEthernet
 

int cnt, rx_len, retEth;

    //char *token;
    //static unsigned char buf[526];
    static struct{
    unsigned char eth_hdr[14];
    int x;
    unsigned char payload[1496];
}__attribute__((packed)) buf;

 
    //retEth = comm_open(DEV_RX1, ETH_RX_PROTO, &sock_rx); /*this function is provided in comm.h from TTTech.*/
    if (retEth == -ENODEV) {
        printf("access to %s failed (you need to be root)\n", DEV_RX1);
        //return retEth;//note
    } else if (retEth) {
        printf("open rx socket error: %d\n", retEth);
        //return retEth;
    }  jstring ret = 0;



 
  printf("befor receive\n");
  unsigned char msg[4000]="";
  char *msgExg;
char *p=NULL;
char *pExg;
int n=1;
//infinite loop for receiving TT-Messages and saving into queue
while(1){
 do{  
    //retEth = comm_recv(sock_rx,(unsigned char *)&buf, sizeof(buf)); /*this function is provided in comm.h from TTTech.*/
  printf("after receive\n");
  printf("retEth= %d\n",retEth);

            if (retEth==1514) {
                p= strchr(buf.payload,'|');
                while ((p != NULL) && (n != 2)){
                      pExg=strdup(p);
                      p=strchr(p+1,'|');
                      if (p != NULL){
                          if((strlen(pExg)-strlen(p) == 1))
                          n=2;
                          free(pExg);}
                }

                strcat(msg, buf.payload);
                      
                
                printf("received %d bytes\n", retEth);
                
                printf("Message is = %s\n and n is %d\n",msg,n);
                //printf("received message = %s\n",token);
            } else if (retEth < 0) {
                printf("read error: %d\n", retEth);
            } else {
                printf("error message : %d\n", retEth);
            }

while (n<2);

                              char delimiter[]="|";
                              char *chkReceiver;
                              char *strchk=strdup(msg);
                              char *rmvReceiverId;

            


                         char recMod[4000];
                         if (buf.x == 0){
                         strcpy(recMod,"nul");
                         chkReceiver= "non";}
                         else if (buf.x == 1){
                         strcpy(recMod,"one");
                         chkReceiver="non"; }
                         else {
                              chkReceiver=strtok(strchk,delimiter);
                              strcpy(recMod,"two");

                              rmvReceiverId= msg+6;
                              strcpy(msg,rmvReceiverId);
                              }
                         if (!strcmp(chkReceiver,"tt102") || !strcmp(chkReceiver,"non")){
                         strcat(recMod,"|");
                         strcat(recMod,msg);
                         strcpy(msg,recMod);
                         printf("Message befor queue is: %s\n",msg);
                         msgExg=strdup(msg);
                         if (!(insert(msgExg))) free(msgExg);}
             

free(strchk);
strcpy(msg,"");

n=n-1;
}
 /* comm_close(sock_rx);
  ret = (*env)->NewStringUTF(env, msg);msgExg,

  */
}

void receiveTT103 ()
{
//recieving messages through TTEthernet
 

int cnt, rx_len, retEth;

    //char *token;
    //static unsigned char buf[526];
    static struct{
    unsigned char eth_hdr[14];
    int x;
    unsigned char payload[1496];
}__attribute__((packed)) buf;

 
    //retEth = comm_open(DEV_RX3, ETH_RX_PROTO, &sock_rx); /*this function is provided in comm.h from TTTech.*/
    if (retEth == -ENODEV) {
        printf("access to %s failed (you need to be root)\n", DEV_RX3);
        //return retEth;//note
    } else if (retEth) {
        printf("open rx socket error: %d\n", retEth);
        //return retEth;
    }  jstring ret = 0;



 
  printf("befor receive\n");
  unsigned char msg[4000]="";
  char *msgExg;
char *p=NULL;
char *pExg;
int n=1;

while(1){
 do{  
   //retEth = comm_recv(sock_rx,(unsigned char *)&buf, sizeof(buf)); /*this function is provided in comm.h from TTTech.*/
  printf("after receive\n");
  printf("retEth= %d\n",retEth);

            if (retEth==1514) {
                p= strchr(buf.payload,'|');
                while ((p != NULL) && (n != 2)){
                      pExg=strdup(p);
                      p=strchr(p+1,'|');
                      if (p != NULL){
                          if((strlen(pExg)-strlen(p) == 1))
                          n=2;
                          free(pExg);}
                }
               /* if (p != NULL){ p=strchr(p+1,'|');
                       if (p != NULL)
                           n=n+1;} */
                strcat(msg, buf.payload);
                      
                
                printf("received %d bytes\n", retEth);
                
                printf("Message is = %s\n and n is %d\n",msg,n);
                //printf("received message = %s\n",token);
            } else if (retEth < 0) {
                printf("read error: %d\n", retEth);
            } else {
                printf("error message : %d\n", retEth);
            }
 // strcpy(newstring, buf.payload)
        //}

    //usleep(30000);
}
while (n<2);

                              char delimiter[]="|";
                              char *chkReceiver;
                              char *strchk=strdup(msg);
                              char *rmvReceiverId;

            


                         char recMod[4000];
                         if (buf.x == 0){
                         strcpy(recMod,"nul");
                         chkReceiver= "non";}
                         else if (buf.x == 1){
                         strcpy(recMod,"one");
                         chkReceiver="non"; }
                         else {
                              chkReceiver=strtok(strchk,delimiter);
                              strcpy(recMod,"two");
                           //    strcat(recMod,"|");
                             // strcat(recMod,"tt102");
                              rmvReceiverId= msg+6;
                              strcpy(msg,rmvReceiverId);
                              }
                         if (!strcmp(chkReceiver,"tt102") || !strcmp(chkReceiver,"non")){
                         strcat(recMod,"|");
                         strcat(recMod,msg);
                         strcpy(msg,recMod);
                         printf("Message befor queue is: %s\n",msg);
                         msgExg=strdup(msg);
                         if (!(insert(msgExg))) free(msgExg);}
             

free(strchk);
strcpy(msg,"");

n=n-1;
}

}


/* the native class which is responsible for listening to TT messages and inserting them to the queue */ 
JNIEXPORT void JNICALL Java_org_universaal_ttelistener_TTEMsgListening_nativeTTEMsgListening(JNIEnv *env, jobject obj)
{

pthread_t threadTTR1, threadTTR3;
int  iretR1, iretR3;
iretR1 = pthread_create( &threadTTR1, NULL,receiveTT101,NULL);
iretR3 = pthread_create( &threadTTR3, NULL,receiveTT103,NULL);
}
/* the native class which is responsible for fetching TT messages from the queue */ 
JNIEXPORT jstring JNICALL Java_org_universaal_ttelistener_TTEMsgFetching_nativeJniFetching (JNIEnv *env, jobject obj)
{
char *tempMsg;
  jstring ret = 0;
  tempMsg= NULL;
  while (tempMsg==NULL){
  tempMsg=remov();
  usleep(1000);
  }
  
  printf("read queue is : %d\n",out_index);
  ret = (*env)->NewStringUTF(env, tempMsg);
  return ret;


}



