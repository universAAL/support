/**Copyright [2011-2014] [University of Siegen, Embedded System Instiute]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 * Header for class org_universAAL_middleware_acl_upnp_importer_JNIFoo
 * @author <a href="mailto:hamzah.dakheel@uni-siegen.de">Hamzah Dakheel</a> 
 * @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
 *		�2012
 */
 
/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class org_universAAL_middleware_acl_upnp_importer_JNIFoo */

#ifndef _Included_org_universAAL_middleware_acl_upnp_importer_JNIFoo
#define _Included_org_universAAL_middleware_acl_upnp_importer_JNIFoo
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     org_universAAL_middleware_acl_upnp_importer_JNIFoo
 * Method:    nativeFoo
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_universAAL_middleware_acl_upnp_importer_JNIFoo_nativeFoo
  (JNIEnv *, jobject, jstring);

#ifdef __cplusplus
}
#endif
#endif
