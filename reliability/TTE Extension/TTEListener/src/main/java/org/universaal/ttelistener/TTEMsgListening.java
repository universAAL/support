/**Copyright [2011-2014] [University of Siegen, Embedded System Instiute]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 * TTE-Patch TTEMsListing Implementation. 
 * @author <a href="mailto:hamzah.dakheel@uni-siegen.de">Hamzah Dakheel</a> 
 * @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
 *		�2012
 */

/*
 *
 *This class is responsible for initiating the Listening thread in TTE-service.
 *
 */
package org.universaal.ttelistener;

import org.universAAL.middleware.acl.upnp.importer.*;


public class TTEMsgListening extends Thread { 
	   
	String payload;
	public TTEMsgListening(String threadName){
		super(threadName);
		start();
	
	}
	public TTEMsgListening(){
	}

	public native void nativeTTEMsgListening();    

	static {
		System.load("/home/rtts/libJniListener.so");
	}    
	
	public void run(){
			nativeTTEMsgListening();			
	}
  
    
	public void startListning(String args) {
	   new TTEMsgListening("TTEMsgListener");

		 new TTEMsgFetching().MsgProcessing();

		}
	}