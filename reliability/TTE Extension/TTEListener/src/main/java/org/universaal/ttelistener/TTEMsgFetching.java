/**Copyright [2011-2014] [University of Siegen, Embedded System Instiute]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 * TTE-Patch TTEMsListing Implementation. 
 * @author <a href="mailto:hamzah.dakheel@uni-siegen.de">Hamzah Dakheel</a> 
 * @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
 *		�2012
 */

/*
 *
 *This class is responsible for initiating the fetching thread in TTE-service
 *
 */
package org.universaal.ttelistener;

import java.util.StringTokenizer;
import org.universAAL.middleware.acl.upnp.importer.*;
import org.universAAL.middleware.acl.upnp.exporter.actions.*;

public class TTEMsgFetching extends Thread {
	
    public native String nativeJniFetching(); 
    static {
        System.load("/home/rtts/libJniListener.so");//uAAL/workspace/TTE001/TTEListener/target/classes/libJniListener.so");
    } 
	
	public TTEMsgFetching(){
		
	}
	
	public TTEMsgFetching(String threadName){
		super(threadName);
		start ();
	}
	
	
	
	public void run(){
		
		String str;
		while (true){
		    str = nativeJniFetching();
		    String msg[]= new String[4] ;
			int i=0;					
			StringTokenizer str1 = new StringTokenizer (str,"|");
			String mod = str1.nextToken();
			while(str1.hasMoreTokens()){
				msg[i]=str1.nextToken();
				
			++i;
			}
			if (mod.equals("nul")||mod.equals("one")){
				new JNIFoo(mod,msg[0]+"|"+msg[1]+"||");
			}
			else	
			(new TTEMessageAction()).invoke(msg[0], msg[1]);
		    }
		}
		
	
	
	public void MsgProcessing(){
		new TTEMsgFetching("TTEMsgFetching");
		
		
	}
	

}