/**Copyright [2011-2014] [University of Siegen, Embedded System Instiute]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 * TTE-Patch TTEMsListing Implementation. 
 * @author <a href="mailto:hamzah.dakheel@uni-siegen.de">Hamzah Dakheel</a> 
 * @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
 *		�2012
 */

/*
 *
 *this class is responsible for initiating the listening channels to TTE network and the fetching process.
 *
 */

package org.universaal.ttelistener;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.universAAL.middleware.acl.upnp.importer.*;

/**
 * 
 * TTE-Patch Listening Activator Implementation.
 * 
 * @author <a href="mailto:hamzah.dakheel@uni-siegen.de">Hamzah Dakheel</a>
 * @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>     
 * 
 */


public class Activator implements BundleActivator{
	public static BundleContext context=null;

	public void start(BundleContext context) throws Exception {
		Activator.context=context;
		System.out.println("Hallo from TTEListener");
		String ham = new TTEMsgHandling().getId();
		new TTEMsgListening().startListning(ham);// strat listening thread.
	}

	public void stop(BundleContext arg0) throws Exception {
	}

}


