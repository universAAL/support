# Copyright [2011-2014] [University of Siegen, Embedded System Instiute]
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#
#	@author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
#   @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
# 		�2012
#

. /lib/lsb/init-functions
    MOD=/home/rtts/tte_pl/examples/demo_server/kernel_module.ko 
    sudo killall -9 server 2>/dev/null
    sudo ifdown eth1 2>/dev/null
    sudo ifconfig eth0 down 2>/dev/null
    sudo ifdown tt102 2>/dev/null
    #sudo ifdown bg0 2>/dev/null
    sudo rmmod r8169 2>/dev/null
    sudo rmmod kernel_module 2>/dev/null
    sudo insmod $MOD
    sleep 2
    sudo modprobe r8169
    sleep 2
    sudo ifup eth1
    sudo ifdown eth1
    sudo ifup eth1
    sleep 2 	   
    sudo ifconfig tt101 -arp up promisc
    sudo ifconfig tt103 -arp up promisc
    sudo ifconfig tt104 -arp up promisc
    sudo ifconfig tt105 -arp up promisc
    # sudo ifup eth0 2>/dev/null
   
    sudo ifup tt102
   # sudo ifup bg0
