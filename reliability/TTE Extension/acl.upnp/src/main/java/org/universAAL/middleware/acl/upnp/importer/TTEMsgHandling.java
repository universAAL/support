/**Copyright [2011-2014] [University of Siegen, Embedded System Instiute]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 * TTE-Patch Modification 
 * @author <a href="mailto:hamzah.dakheel@uni-siegen.de">Hamzah Dakheel</a> 
 * @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
 *		�2012
 */
      
  
/*
 *  TTE-Patch Modification 
 *  this class make multiple essential jobs. It�s responsible for setting,
 *  sending  the coupled ID, saving the coupled ID of remote peers,
 *  additionally, it�s responsible for delivering the native class with
 *  TT-Messages to send them upon TTE-Network
 * 
 */  
package org.universAAL.middleware.acl.upnp.importer;

import java.util.StringTokenizer;

/**
 * 
 * TTE-Patch Listening Activator Implementation.
 * 
 * @author <a href="mailto:hamzah.dakheel@uni-siegen.de">Hamzah Dakheel</a>
 * @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>     
 * 
 */

public class TTEMsgHandling { 
	   
	    static int n = 0;
	    static String MyId ; // to save a unique copy of the coupled ID
	    static String[] remotePeersId = new String[4]; // to save a coupled IDs for other peers
	    public TTEMsgHandling(){
         System.out.println(n);
	    }
	    
	    public TTEMsgHandling(String Id){
	    	TTEMsgHandling.MyId= "tt103"+"|"+Id+"||"; // setting the local coupled ID
	    	
	    }
	    public TTEMsgHandling(String mod, String remoteId){
	    	TTEMsgHandling.remotePeersId[n]= remoteId;
	    	if (mod.equals("nul"))
	    		this.sendIdMsg("one");
	    	n=n+1;
	    	System.out.println(n);
	    }
	    
	    public native String nativeFoo(String ham);    

	    static {
		System.load("/home/rtts/libfoo.so"); // loading the native class that is responsible for msgs. transmitting within TTE-Patch
	    }        
	    
	    public synchronized int sendTTMsg (String msg) {
	    	String str="";
	    	int x =0;
			str = nativeFoo(msg);
			if (str != null){
			x=1;
			}
			return x;
			}
	    
	    
	    public String getId(){
	    	return MyId;
	    }
	    
	    public void sendIdMsg(String msgMod){
	    	
	    String IdMsg;
	    IdMsg= TTEMsgHandling.MyId;
      	IdMsg= msgMod + "|" + IdMsg;
	   	new TTEMsgHandling().sendTTEMsg(IdMsg);
	    }
	    
	    public String getTTEId(String SodaPopPeerId){
	    	int i = 0;
	    	String TTEId = "";
	    	 String[] str = new String[2];
	    	for (int j=0; j<n;j=j+1 ){
			StringTokenizer str1 = new StringTokenizer (TTEMsgHandling.remotePeersId[j],"|");
			System.out.println(TTEMsgHandling.remotePeersId[j]);
           
			while(str1.hasMoreTokens()){
				str[i]=str1.nextToken();
				
			++i;
			}
			if (SodaPopPeerId.equals(str[1])){
				TTEId=str[0];
			}
			i=0;
	    	}
	    	return TTEId;
	    }
	    
	    
	    public String getSodaPopPeerId (String TTEId){
	    	
	    	int i = 0;
	    	String SodaPopPeerId = "";
	    	for (int j=0; j<n;j=j+1 ){
			StringTokenizer str1 = new StringTokenizer (TTEMsgHandling.remotePeersId[j],"|");
            String[] str = new String[2];
			while(str1.hasMoreTokens()){
				str[i]=str1.nextToken();
				
			++i;
			}
			if (TTEId.equals(str[0])){
				SodaPopPeerId=str[1];
			}
	    	}
	    	return SodaPopPeerId;
	    	
	    }
	    
	    public static int main(String[] msg) {
	    String msgMod = "two";						
	    msg[0]= (new TTEMsgHandling().getTTEId(msg[0]));
	    
	    String msgCmp=msgMod + "|" + msg[0] +"|"+msg[1]+"|"+msg[2]+"||";
	    int x=(new TTEMsgHandling()).sendTTMsg(msgCmp);
	    return x;
	    
	    }
}