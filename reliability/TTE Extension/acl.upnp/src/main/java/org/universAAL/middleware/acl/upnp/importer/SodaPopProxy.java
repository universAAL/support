/**Copyright [2011-2014] [University of Siegen, Embedded System Instiute]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 * TTE-Patch Modification 
 * @author <a href="mailto:hamzah.dakheel@uni-siegen.de">Hamzah Dakheel</a> 
 * @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
 *		�2012
 */
 
 /* TTE-Patch Modification
  *
  *         - processesBusMessage() method has been modified to be able to send the messages as TT-Messages over TTE-Patch. 
  *           If the TT-Service was not available then msgs.should be transmitted over upnp protocol
  *
  */
package org.universAAL.middleware.acl.upnp.importer;

import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.service.upnp.UPnPDevice;
import org.osgi.service.upnp.UPnPService;
import org.universAAL.middleware.acl.upnp.exporter.actions.JoinBusAction;
import org.universAAL.middleware.acl.upnp.exporter.actions.LeaveBusAction;
import org.universAAL.middleware.acl.upnp.exporter.actions.NoticePeerBussesAction;
import org.universAAL.middleware.acl.upnp.exporter.actions.PrintStatusAction;
import org.universAAL.middleware.acl.upnp.exporter.actions.ProcessBusMessageAction;
import org.universAAL.middleware.acl.upnp.exporter.actions.ReplyPeerBussesAction;
import org.universAAL.middleware.acl.upnp.exporter.services.SodaPopPeerService;

import org.universAAL.middleware.acl.SodaPopPeer;

/* 
 * The SodaPopProxy acts as proxy for remote peers exported through the UPnP protocol. The methods of the SodaPopPeer interface are here implemented by invoking accordingly the UPnP actions 
 * on the target UPnP device representing the peer. Generally invokin an action on the UPnP device corresponds to :
 * 1)prepare a Dictionary object with all the relevant properties
 * 2)fetch the object modeling the required UPnP action
 * 3)invoke the action by passing the Dictionary previously created.
* @author <a href="mailto:francesco.furfari@isti.cnr.it">Francesco Furfari</a>
*/

public class SodaPopProxy implements SodaPopPeer{
    private final String UUID_PREFIX = "uuid:";
	private UPnPService sodapopPeerService;
	private String id;
	
	/**
	 * Proxy constructor. The UPnPDevice parameter is used in order to fetch all the required references able to
	 * interact with the remote peer.  
	 * @param device
	 */
	public SodaPopProxy(UPnPDevice device){
		sodapopPeerService = device.getService(SodaPopPeerService.SERVICE_ID);		
		id  = (String) device.getDescriptions(null).get(UPnPDevice.ID);
		id = id.substring(UUID_PREFIX.length());
	}

	public String getID() {
		//System.out.println("REMOTE_PEER:: getID invoked (cached)");
		return id;
//		try {
//			
//			System.out.println("REMOTE_PEER:: getID invoked");
//			Dictionary dictionary = sodapopPeerService.getAction(GetIdAction.NAME).invoke(null);
//			String id = (String) dictionary.get(GetIdAction.RESULT_ID);
//			System.out.println("REMOTE_PEER:: getID returned:" +id);
//			return id;
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return null;
	}

	public void joinBus(final String busName, final String joiningPeer) {
//		new Thread(){
//			public void run(){
				try {
					//System.out.println("REMOTE_PEER:: joinBus invoked:" +busName + ", " + joiningPeer);
					Dictionary dictionary = new Hashtable();
					dictionary.put(JoinBusAction.BUS_NAME, busName);
					dictionary.put(JoinBusAction.JOINING_PEER, joiningPeer);
					sodapopPeerService.getAction(JoinBusAction.NAME).invoke(dictionary);
					return;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//			}
//		}.start();
		return ;	
	}

	public void leaveBus(final String busName,final  String leavingPeer) {
//		new Thread(){
//			public void run(){
				try {
					//System.out.println("REMOTE_PEER:: leaveBus invoked:" +busName + ", " + leavingPeer);
					Dictionary dictionary = new Hashtable();
					dictionary.put(LeaveBusAction.BUS_NAME, busName);
					dictionary.put(LeaveBusAction.LEAVING_PEER, leavingPeer);
					sodapopPeerService.getAction(LeaveBusAction.NAME).invoke(dictionary);
					return;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//			}
//		}.start();
		return ;	

	}

	public void noticePeerBusses(final String peerID, final String busNames) {
//		new Thread(){
//			public void run(){
				try {
					//System.out.println("REMOTE_PEER:: noticePeerBusses invoked:" +peerID + ", " + busNames);
					Dictionary dictionary = new Hashtable();
					dictionary.put(NoticePeerBussesAction.PEER_ID, peerID);
					dictionary.put(NoticePeerBussesAction.BUSSES_NAME, busNames);
					sodapopPeerService.getAction(NoticePeerBussesAction.NAME).invoke(dictionary);
					return;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//			}
//		}.start();
		return ;	
	}
	
	public void replyPeerBusses(String peerID, String busNames) {
//		new Thread(){
//			public void run(){
				try {
					//System.out.println("REMOTE_PEER:: replyPeerBusses invoked:" +peerID + ", " + busNames);
					Dictionary dictionary = new Hashtable();
					dictionary.put(ReplyPeerBussesAction.PEER_ID, peerID);
					dictionary.put(ReplyPeerBussesAction.BUSSES_NAME, busNames);
					sodapopPeerService.getAction(ReplyPeerBussesAction.NAME).invoke(dictionary);
					return;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//			}
//		}.start();
		
	}

  public void processBusMessage(final String busName,final String msg) {
//		new Thread(){
//			public void run(){
				try {
					
					Dictionary dictionary = new Hashtable();
					dictionary.put(ProcessBusMessageAction.BUS_NAME, busName);
					dictionary.put(ProcessBusMessageAction.MESSAGE, msg);
					String receiverId = this.id;  //getting the remote peer ID
					String[] sentMsg ={receiverId,busName,msg}; //combining the msg. elements in one string
					int x =TTEMsgHandling.main(sentMsg);  //deliver the msg for sending upon TT
					//check the activation of TTE-services if it is not available then the msg. will be send upon UPnP-Connector.
		            if (x==0){
					sodapopPeerService.getAction(ProcessBusMessageAction.NAME).invoke(dictionary);}
					return;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//			}
//		}.start();
		return ;	
	}

	public void printStatus() {
		Dictionary dictionary = new Hashtable();
		try {
			sodapopPeerService.getAction(PrintStatusAction.NAME).invoke(dictionary);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
