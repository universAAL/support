/**Copyright [2011-2014] [University of Siegen, Embedded System Instiute]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 * TTE-Patch Modification 
 * @author <a href="mailto:hamzah.dakheel@uni-siegen.de">Hamzah Dakheel</a> 
 * @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
 *		�2012
 */


/*TTE-Patch Modification 
 *     in this class an apdated image for the sodapod local instance will be saved. 
 *     then invoke function will be called by processBusMessageAction () function
 *     to use the stored image and enter sodapop layer
 */
package org.universAAL.middleware.acl.upnp.exporter.actions;

import org.universAAL.middleware.acl.SodaPopPeer;
import org.universAAL.middleware.acl.upnp.importer.*;

/**
 * 
 * TTE-Patch TTEMsgAction Implenetation
 * 
 * @author <a href="mailto:hamzah.dakheel@uni-siegen.de">Hamzah Dakheel</a>
 * @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>     
 * 
 */

public class TTEMessageAction {
	public static SodaPopPeer LOCALPEER;
	public static String[] dispatcher = new String[4];
	public SodaPopPeer localPeer;
	static int cnt = 0;
	public TTEMessageAction(SodaPopPeer DlocalPeer){
		TTEMessageAction.LOCALPEER=DlocalPeer;
	}


	public TTEMessageAction(){
		
		
	}
	
	
	
	//public synchronized void invoke (String TTEId, String busName, String msg)
	public synchronized void invoke (String busName, String msg){
	
		this.localPeer = LOCALPEER;
		localPeer.processBusMessage(busName, msg);
	}
}
