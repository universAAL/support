/**Copyright [2011-2014] [University of Siegen, Embedded System Instiute]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 * TTE-patch Sender native class Implementation 
 * @author <a href="mailto:hamzah.dakheel@uni-siegen.de">Hamzah Dakheel</a> 
 * @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
 *		�2012
 */


/*foo.c is a JNI native class which is resposible for receiving the messages
 *from java classes (processBusMessageAction (),...) and processes them to 
 *send them finaly upon TTE network.
 
 *comm.h, common.h and debug.h files are a TTTech libraries which is reposible for manipulating 
 *the frames within TTE network by using raw socket technique.
 *In order to uncomment this library and its related function and reuse it again
 *you should have the licenses from TTTEch.*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jni.h>
#include "org_universAAL_middleware_acl_upnp_importer_JNIFoo.h"

#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sched.h>
#include <sys/time.h>
#include <linux/if_ether.h>

//#include "common.h" 
//#include "debug.h"  
//#include <comm.h>   
//raw socket headers
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/if_packet.h>
#include <linux/if_arp.h>
#include <arpa/inet.h>

static struct {
    int sock;
    int if_index;
    int used;
} socks[3] = {
    {.used = 0},
    {.used = 0},
    {.used = 0},
};

JNIEXPORT jstring JNICALL Java_org_universAAL_middleware_acl_upnp_importer_JNIFoo_nativeFoo (JNIEnv *env, jobject obj, jstring ham)
{
  int i;
  int ds_ret;
  jboolean isCopy;
  unsigned char* newstring;
 
  static int tt_tx_sock=-1;

  jstring ret = 0;

  newstring = (char*)malloc(4000);
 
  if(newstring == NULL)
  {     
      return ret;
  }
 
  memset(newstring, 0, 4000); 
  
 
  newstring = (*env)->GetStringUTFChars(env, ham, &isCopy);
 
//"foo: Test program of JNI.\\n";
 printf("Thi is from Jni %d\n", strlen(newstring));
 

//sending the chars through TTEthernet
  int retEth;

  printf("befor enter\n");
    enter();
	printf("befor open\n");
    // retEth=comm_open("tt101",ETH_P_ALL,&tt_tx_sock);  /*this function is provided in comm.h from TTTech.*/
    if (retEth) {
        error("comm_open() failed with %d\n", ret);
        leave(return ret);
    } 

	

        int msgsize=strlen(newstring);
        int stdsize=1496; //specifying the msg size exactly as mentioned in the configuration file of each end-system.
        int n=msgsize/stdsize;
        int mod=msgsize % stdsize;
        if (mod>0)
        n=n+1;
        printf("n= %d\n",n);
       
 

    static struct {
        unsigned char eth_hdr[14];
        int x;
        unsigned char payload[1496];

    } __attribute__((packed)) net_pkt = {
        .eth_hdr = {
            0x03, 0x03, 0x04, 0x00, 0x00, 0x08,
            0x02, 0x02, 0x22, 0x22, 0x00, 0x08,
            0xBA, 0xEA,  // dummy frame type
        }
    };
    
    
    char delimiter[]="|";
    char *sendMod;
    char *str=strdup(newstring); 
   
    sendMod=strtok(str,delimiter); 
    printf("this is mod %s\n",sendMod);
    if (!strcmp(sendMod,"nul"))  
    net_pkt.x=0;
    else if(!strcmp(sendMod,"one")) 
    net_pkt.x=1;
    else
    net_pkt.x=2;
   free(str);

    newstring = newstring + 4;
      
    printf("this correct: %s\n",newstring);
    net_pkt.eth_hdr[3]  = 0x06;  // destination address (TT)
    net_pkt.eth_hdr[11] = 0x01;  // source address (=switch port)
    net_pkt.eth_hdr[13] = 0x88;  // ETH type

   // sending TT-Messages in several bunches in case of messages with huge size   
   for(i=0; i<n; ++i){
     strncpy (net_pkt.payload, &newstring[i*stdsize], stdsize);
     printf("befor sending\n");
     //comm_send(tt_tx_sock, (unsigned char *)&net_pkt, sizeof(net_pkt));  /*this function is provided in comm.h from TTTech.*/
      printf("after sending\n");
     usleep(10000);

     }
//ending sending operation

    comm_close(tt_tx_sock);

  ret = (*env)->NewStringUTF(env, newstring); // convert char variable to jstring to be handled by java
  
 // free(newstring);
         printf("end of Jni!!!!!!!\n");
  return ret;
}


