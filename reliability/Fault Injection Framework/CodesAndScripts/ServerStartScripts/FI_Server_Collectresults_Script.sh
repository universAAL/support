# Copyright [2011-2014] [University of Siegen, Embedded System Instiute]
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

#  @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
# 		�2012
#

mv /home/rtts/operation101/eventReceive1.txt /home/rtts/results/eventReceive1Node1.txt
mv /home/rtts/operation101/eventReceive2.txt /home/rtts/results/eventReceive2Node1.txt
mv /home/rtts/operation101/eventSend1.txt /home/rtts/results/eventSend1Node1.txt

mv /home/rtts/operation102/eventReceive1.txt /home/rtts/results/eventReceive1Node2.txt
mv /home/rtts/operation102/eventReceive2.txt /home/rtts/results/eventReceive2Node2.txt
mv /home/rtts/operation102/eventSend2.txt /home/rtts/results/eventSend2Node1.txt

mv /home/rtts/operation103/eventReceive1.txt /home/rtts/results/eventReceive1Node3.txt
mv /home/rtts/operation103/eventReceive2.txt /home/rtts/results/eventReceive2Node3.txt

mv /home/rtts/operation104/eventReceive1.txt /home/rtts/results/eventReceive1Node4.txt
mv /home/rtts/operation104/eventReceive2.txt /home/rtts/results/eventReceive2Node4.txt

mv /home/rtts/operation105/eventReceive1.txt /home/rtts/results/eventReceive1Node5.txt
mv /home/rtts/operation105/eventReceive2.txt /home/rtts/results/eventReceive2Node5.txt
