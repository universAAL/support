# Copyright [2011-2014] [University of Siegen, Embedded System Instiute]
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

#  @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
# 		�2012
#

#This file contains the switch off command for the TTEthernet devices, these devices names must be changed according to the opupdate.sh file

   echo "Stopping devices eth0, bg0, tt100..."
    sudo ifdown eth1  down   2>/dev/null
    sudo ifdown tt101   2>/dev/null
    sudo ifconfig tt102 down   2>/dev/null
    sudo ifconfig tt103 down   2>/dev/null
    sudo ifconfig tt104 down   2>/dev/null
    sudo ifconfig tt105 down   2>/dev/null

    echo "removing modules ttethernet..."
    sudo rmmod video_server 2>/dev/null
    echo "inserting module ..."
    sudo modprobe r8169
    echo "waiting until r8169 is ready..."
    #important: otherwise the following ifconfig will not work correctly
    sleep 4
    echo "Starting device eth1 ..."
    sudo ifup eth1 up
    sudo reboot
