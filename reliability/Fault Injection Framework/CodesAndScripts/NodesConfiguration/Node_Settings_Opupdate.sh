# Copyright [2011-2014] [University of Siegen, Embedded System Instiute]
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

#  @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
# 		�2012
#

# This file applies to Node 1 for other nodes pleas follow the manual steps.

# This file contains the initiation for the TTEthernet devices and kernel mode, and the mount process using the nfs server.
# 	As in the interface file the devices used for the TTEthernet communication must be changed according to which one of the
#	nodes is used for sending a message, and which one of them is used to receive
#
. /lib/lsb/init-functions
  MOD=/home/rtts/tte_pl/examples/demo_server/video_server.ko
    sudo killall -9 server 2>/dev/null
    sudo ifdown eth0 2>/dev/null
    sudo ifconfig eth0 down 2>/dev/null
    sudo ifdown tt101 2>/dev/null
    sudo rmmod r8169 2>/dev/null
    sudo rmmod video_server 2>/dev/null
    sudo insmod $MOD
    sleep 2
    sudo modprobe r8169
    sleep 2
    sudo ifup eth1
    sudo ifdown eth1
    sudo ifup eth1
    sleep 2
    sudo ifconfig tt102 -arp up promisc
    sudo ifconfig tt103 -arp up promisc
    sudo ifconfig tt104 -arp up promisc
    sudo ifconfig tt105 -arp up promisc
    sudo ifup tt101
    sudo modprobe nfs
    sudo mount -t nfs4 -o proto=tcp,port=2049 10.0.0.100:/ /mnt
    sudo rm -r /home/rtts/operation/mnt
    sudo cp -r /mnt /home/rtts/operation
