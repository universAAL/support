/**Copyright [2011-2014] [University of Siegen, Embedded System Instiute]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 * Fault Injection Framework Receiver application for nodes
 * @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
 *		�2011
 */
 
/*
 * Fault Injection Framework Receiver application for nodes
 
 There are several scenarios for fault injection that can be implemented:
	-Changing the pause time, this will affect the events waiting time in the events queue inside the bus..
	-Disturbing the bus by creating faulty behaviour in one node, this will lead the node to connect and disconnect to the bus for several times aimlessly.
	-Change the number of sending nodes and their event sending rate.
	-Stop a node during the receiving process.
	-Initiating multiple context subscriber or context publisher
*/
 
 
package org.universAAL.SendReceive.Receive;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import org.osgi.framework.BundleContext;
import org.universAAL.middleware.context.ContextEvent;
import org.universAAL.middleware.context.ContextEventPattern;
import org.universAAL.middleware.context.ContextSubscriber;
import org.universAAL.middleware.sodapop.msg.Message;


public class CSubscriber extends ContextSubscriber{

public int flag=0,i=0,j1=0,t11,end1=0,end2=0,j2=0,t22=0;
public long ts1,ts2,te1,te2;


        protected CSubscriber(BundleContext context,
                        ContextEventPattern[] initialSubscriptions) {
                super(context, initialSubscriptions);
                // TODO Auto-generated constructor stub
        }

        protected CSubscriber(BundleContext context) {
                super(context, getPermanentSubscriptions());
                // TODO Auto-generated constructor stub
        }

        private static ContextEventPattern[] getPermanentSubscriptions() {
                // TODO Auto-generated method stub
                return null;
        }

        public void communicationChannelBroken() {
                // TODO Auto-generated method stub

        }

        //public void handleContextEvent(ContextEvent event) {
                // TODO Auto-generated method stub
                //
        //}
 

	   public void handleContextEvent(ContextEvent event) {
		   
		   flag=Integer.parseInt((String) event.getRDFObject()) % 100;     
		   i = Integer.parseInt((String) event.getRDFObject()); // reading the event flag to check its type and source 
			   
		   if (flag ==1){
			   switch(i){
				   case 201:
					   j1=0;
					   t11++;
					   ts1 =  System.currentTimeMillis();
					   break;
				   case 101:
					   j1++;
					   break; 
				   case 301:
					   te1 =  System.currentTimeMillis()-15000;
					  try {
							BufferedWriter output;
							output = new BufferedWriter(new FileWriter("eventReceive1.txt",true));
							output.newLine();
							output.append(Integer.toString(t11)+"	"+Long.toString(ts1)+"	"+Long.toString(te1)+"	"+Integer.toString(j1));
							output.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					   break; 
				   case 401: end1=10;
				   if (end2==10) {
					   Activator.waiting(30000);
					   System.exit(1);
				   }
				   break; 
				   default: System.out.println("invalid event")  ; break;                    	      
				   }
			}
		   if (flag ==2){
			   switch(i) {
				   case 202:
					   j2=0;
					   t22++;
					   ts2 =  System.currentTimeMillis();
					   break;
				   case 102:
					   j2++;
					   break; 
				   case 302:
					   te2 =  (System.currentTimeMillis()-15000);
					  try {
							BufferedWriter output;
							output = new BufferedWriter(new FileWriter("eventReceive2.txt",true));
							output.newLine();
							output.append(Integer.toString(t22)+"	"+Long.toString(ts2)+"	"+Long.toString(te2)+"	"+Integer.toString(j2));
							output.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					   break; 
				   case 402: end2=10;
				   if (end1==10){
					   Activator.waiting(30000);
					   System.exit(1);
				   }
				   break; 
				   default: System.out.println("invalid event")  ; break;                    	      
				   }
			}
			   
		}

        public void end(){
                ;
        }

}

