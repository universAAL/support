/**Copyright [2011-2014] [University of Siegen, Embedded System Instiute]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 * Fault Injection Framework publisher application for nodes
 * @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
 *		�2011
 */

/*
 * Fault Injection Framework publisher application for nodes
 
 There are several scenarios for fault injection that can be implemented:
	-Changing the pause time, this will affect the events waiting time in the events queue inside the bus..
	-Disturbing the bus by creating faulty behaviour in one node, this will lead the node to connect and disconnect to the bus for several times aimlessly.
	-Change the number of sending nodes and their event sending rate.
	-Stop a node during the receiving process.
	-Initiating multiple context subscriber or context publisher
*/

package org.universAAL.SendReceive.Send;


import java.io.*;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.universAAL.middleware.context.ContextPublisher;
import org.universAAL.middleware.context.owl.ContextProvider;
import org.universAAL.middleware.context.owl.ContextProviderType;
import org.universAAL.middleware.context.ContextSubscriber;
import org.universAAL.middleware.context.DefaultContextPublisher; 
import org.universAAL.middleware.context.ContextEvent; // <- NEW LINE
import org.universAAL.middleware.context.ContextEventPattern; // <- NEW LINE
import org.universAAL.ontology.phThing.Device; // <- NEW LINE
import org.universAAL.middleware.owl.supply.LevelRating;
import org.universAAL.middleware.context.ContextEventPattern;

public class Activator implements BundleActivator 
{
   ContextPublisher myContextPublisher; 
   ContextPublisher myContextPublisher2; 
   ContextProvider myContextProvider;
   ContextEvent ev1;
   public double randNumber;
   ContextEventPattern cep = new ContextEventPattern();
   String myURI = "http://ontology.persona.ima.igd.fhg.de/lighting.owl#mydummy";
   String mes="Tempreture for Node 1";

   
public static void waiting (int n){
	        
	        long t0, t1;

	        t0 =  System.currentTimeMillis();

	        do{
	            t1 = System.currentTimeMillis();
	        }
	        while ((t1 - t0) < (n));
	    }
	 
   public void start(BundleContext myBundleContext) throws Exception 
   {

waiting(50000);
      this.myContextProvider = new ContextProvider(this.myURI);
      this.myContextProvider.setType(ContextProviderType.gauge);

  
     
      Device dev=new Device();
      Device dev2=new Device();
      Device dev3=new Device();
      Device dev4=new Device();
      
      dev.setBatteryLevel(LevelRating.high);
      dev2.setBatteryLevel(LevelRating.high);
      dev3.setBatteryLevel(LevelRating.high);
      dev4.setBatteryLevel(LevelRating.high);
      
      dev.setResourceLabel("101");
      dev2.setResourceLabel("201");
      dev3.setResourceLabel("301");
      dev4.setResourceLabel("401");


       ContextEvent ev=new ContextEvent(dev,Device.PROP_RDFS_LABEL);
       ContextEvent ev2=new ContextEvent(dev2,Device.PROP_RDFS_LABEL);
       ContextEvent ev3=new ContextEvent(dev3,Device.PROP_RDFS_LABEL);       
       ContextEvent ev4=new ContextEvent(dev4,Device.PROP_RDFS_LABEL);  
       int i = 90;   // number of loops 
       int w =5000;  // total Time period for sending the events in ms 
       int delay =0 ;// delay between sending events 
       int ne=0 ; //number of event have been sent 
       int pause=15000; 
       long ts,te; 
       int j=0;
	   this.myContextPublisher2 = new DefaultContextPublisher(myBundleContext, this.myContextProvider);   

	   this.myContextPublisher = new DefaultContextPublisher(myBundleContext, this.myContextProvider);   
	   
       this.myContextPublisher2.publish(ev2);  
	   ts =  System.currentTimeMillis(); //record the sending starting time      
       do {
    	   waiting(delay);
    	   this.myContextPublisher.publish(ev); // sending the event to the bus 
    	   ne++;
    	   te =  System.currentTimeMillis(); // record the time after sending the event   
       }
      while (te-ts <w);  
    	
		j++;
		waiting(pause);
		myContextPublisher.close();
		this.myContextPublisher2.publish(ev3); // sending the end event to the bus 
		te =  System.currentTimeMillis()-pause; 

		BufferedWriter output;
		output = new BufferedWriter(new FileWriter("eventSend1.txt",true));
		output.newLine();
		output.append(Integer.toString(j)+"	"+Long.toString(ts)+"	"+Long.toString(te)+"	"+Integer.toString(ne));
		output.close();
		waiting(pause);
		ne=0;
		this.myContextPublisher2.publish(ev4); // sending the end event to the bus 
		myContextPublisher2.close();

   }
	
   public void stop(BundleContext myBundleContext) throws Exception 
   {
	   myContextPublisher2.close();
   }
}
