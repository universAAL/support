/**Copyright [2011-2014] [University of Siegen, Embedded System Instiute]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 * Fault Injection Framework Activator application for the server-controller
 * @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
 *		�2011
 */

/*
 * Fault Injection Framework Activator application for the server-controller
 
 There are several scenarios for fault injection that can be implemented:
	-Changing the pause time, this will affect the events waiting time in the events queue inside the bus..
	-Disturbing the bus by creating faulty behaviour in one node, this will lead the node to connect and disconnect to the bus for several times aimlessly.
	-Change the number of sending nodes and their event sending rate.
	-Stop a node during the receiving process.
	-Initiating multiple context subscriber or context publisher
*/
 
package org.universAAL.SendReceive.Receive;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


import org.universAAL.middleware.context.ContextPublisher;
import org.universAAL.middleware.context.ContextBus;
import org.universAAL.middleware.context.owl.ContextProvider;
import org.universAAL.middleware.context.ContextEvent; 
import org.universAAL.middleware.context.ContextEventPattern;
import org.universAAL.middleware.container.ModuleContext;
import org.universAAL.middleware.container.osgi.uAALBundleContainer;

import org.universAAL.middleware.sodapop.msg.Message;
import org.universAAL.middleware.owl.Restriction;

public class Activator implements BundleActivator 
{
   ContextPublisher myContextPublisher; 
   ContextProvider myContextProvider;
   ContextEvent ev1;
   public ContextBus cp;
   public static ModuleContext mc;
   public static CSubscriber csubscriber=null; 
   public ContextEventPattern[] cep ;

   String myURI = "http://ontology.universaal.org/Lighting.owl#LightSource";
   String mes="hello world";
	public Message m=null; 

	public static void waiting (int n){
        
        long t0, t1;

        t0 =  System.currentTimeMillis();

        do{
            t1 = System.currentTimeMillis();
        }
        while ((t1 - t0) < (n));
    }
 
   public void start(BundleContext myBundleContext) throws Exception 
   {

		waiting(25000);	
		ContextEventPattern cep2 = new ContextEventPattern();
		cep2.addRestriction(Restriction.getAllValuesRestriction(
			ContextEvent.PROP_RDF_SUBJECT,"myURI"));
		cep = new ContextEventPattern[] {cep2};
		
       csubscriber=new CSubscriber(myBundleContext, cep);
 
   }
   
	public void stop(BundleContext context) throws Exception {
			csubscriber.close();

	}
}
