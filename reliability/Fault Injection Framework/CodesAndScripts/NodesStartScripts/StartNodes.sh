# Copyright [2011-2014] [University of Siegen, Embedded System Instiute]
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

#  @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
# 		�2012
#


#!/bin/bash

. /lib/lsb/init-functions
#exit
VIDEO=/home/demo/tttech_3000kb_mjpeg.mp4
PROG=/home/rtts/tte_pl/examples/demo_server/read200/read200

function do_start() 
{
	log_action_begin_msg "Starting the TTE video server"
	killall -9 server 2>/dev/null || true
	cp $VIDEO /tmp/video_file
	sudo ifdown eth0 2>/dev/null || true
	#sudo ifdown eth1 2>/dev/null || true
	sudo ifdown eth1
	sudo ifdown bg0 2>/dev/null || true
	sudo ifdown tt100 2>/dev/null || true
	sudo ifdown tt200 2>/dev/null || true
	sudo rmmod r8169 2>/dev/null || true
	sudo rmmod video_server 2>/dev/null || true
	sudo insmod /home/demo/video_server.ko
	sleep 2
	sudo modprobe r8169
	sleep 2
	sudo ifup eth1
	sudo ifdown eth1
	sudo ifup eth1
	sleep 2
	sudo ifup bg0
	sudo ifup tt100
	sudo ifconfig tt200 up 
	sleep 2
	$PROG #tt100 eth1 /tmp/video_file >/tmp/video.log 2>/tmp/video.err --timer &
	log_action_end_msg 0
}

function do_stop() 
{
	log_action_begin_msg "Stopping the TTE video server"
	killall -9 server 2>/dev/null || true
	rm -f /tmp/video_file
	sudo ifdown bg0 2>/dev/null || true
	sudo ifdown tt100 2>/dev/null || true
	sudo ifdown tt200 2>/dev/null || true
	sudo rmmod video_server 2>/dev/null || true
	sudo rmmod r8169 2>/dev/null || true
	sudo modprobe r8169 2>/dev/null || true
	sleep 2
	sudo modprobe r8169
	sleep 2
	sudo ifup eth1
	sudo ifdown eth1
	sudo ifup eth1
	sleep 2
        sudo ifdown eth0 2>/dev/null || true
	sudo ifup eth0 2>/dev/null || true
	log_action_end_msg 0
}

case "$1" in
  start|"")
	do_start
	;;
  restart)
	do_start
	exit 3
	;;
  stop)
	do_stop
	;;
  *)
	echo "Usage: $0 [start|stop|restart]" >&2
	exit 3
	;;
esac
