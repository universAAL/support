# Copyright [2011-2014] [University of Siegen, Embedded System Instiute]
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

#  @author <a href="mailto:zaher.owda@uni-siegen.de">Zaher Owda</a>  
# 		�2012
#

cp /mnt/ReadyToRun/lastUpdate/SendExample2-0.1-0.SNAPSHOT.jar /home/rtts/uAAL/ReadyToRun/SendReciveExample2/bundles/
cp /mnt/ReadyToRun/lastUpdate/SendExample_0.1-0.SNAPSHOT.jar /home/rtts/uAAL/ReadyToRun/SendReceiveExample1/bundles/
cp /mnt/ReadyToRun/lastUpdate/ReceiveExample-0.1-0.SNAPSHOT.jar /home/rtts/uAAL/ReadyToRun/ReceiveExample/bundles/
cp /mnt/ReadyToRun/lastUpdate/ReceiveExample-0.1-0.SNAPSHOT.jar /home/rtts/uAAL/ReadyToRun/SendReceiveExample1/bundles/  
cp /mnt/ReadyToRun/lastUpdate/ReceiveExample-0.1-0.SNAPSHOT.jar /home/rtts/uAAL/ReadyToRun/SendReciveExample2/bundles/
cp /mnt/ReadyToRun/lastUpdate/SendExample_0.1-0.SNAPSHOT.jar /home/rtts/uAAL/ReadyToRun/SendExample/ 

cd /home/rtts/uAAL/ReadyToRun/SendReceiveExample1/
java -Dosgi.noShutdown=true -Dorg.universAAL.middleware.peer.is_coordinator=false -Dorg.universAAL.middleware.peer.member_of=urn:org.universAAL.aal_space:test_env -Dbundles.configuration.location=/home/rtts/uAAL/ReadyToRun/SendReceiveExample1/conf -jar /home/rtts/uAAL/ReadyToRun/SendReceiveExample1/bin/felix.jar

cp /home/rtts/uAAL/ReadyToRun/SendReceiveExample1/eventSend1.txt /mnt
 cp /home/rtts/uAAL/ReadyToRun/SendReceiveExample1/eventReceive2.txt /mnt
 cp /home/rtts/uAAL/ReadyToRun/SendReceiveExample1/eventReceive1.txt /mnt
#rm /home/rtts/uAAL/ReadyToRun/SendReceiveExample1/eventSend1.txt
#rm /home/rtts/uAAL/ReadyToRun/SendReceiveExample1/eventReceive2.txt
#rm /home/rtts/uAAL/ReadyToRun/SendReceiveExample1/eventReceive1.txt

