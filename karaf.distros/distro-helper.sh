#!/bin/bash

WIN_DISTRO="karaf4uAAL2.2.9-WIN.zip"
LINUX_DISTRO="karaf4uAAL2.2.9-Linux.tar.gz"

WIN_DIR="karaf4uAAL2.2.9-WIN"
LINUX_DIR="karaf4uAAL2.2.9-Linux"

function unpack {
	unzip -q $WIN_DISTRO
	tar -xzf $LINUX_DISTRO
	exit 0;
}

function pack {
	if [ -d "$WIN_DIR" ]; then
		rm "${WIN_DISTRO}"
		rm -rf ${WIN_DIR}/data/*
		zip -rq9 "${WIN_DISTRO}" "${WIN_DIR}" || exit 1
		rm -rf "${WIN_DIR}"
	fi
	if [ -d "$LINUX_DIR" ]; then
		rm "${LINUX_DISTRO}"
		rm -rf ${LINUX_DIR}/data/*
		tar -czhf "${LINUX_DISTRO}" "${LINUX_DIR}" || exit 1
		rm -rf "${LINUX_DIR}"
	fi
	exit 0;
}

case $1 in
	"unpack" ) unpack ;;
	"pack" ) pack ;;
	* ) echo "use $0 <unpack|pack>" ; exit 1;;
esac

