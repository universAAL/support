GOAL
----
This file describe the content of this folder. 


CONTENT DESCRIPTION
-------------------
 - distro-helper.sh is a simple BASH script for creaing a tar.gz and zip archive of the karaf4uAAL2.2.9-Linux and karaf4uAAL2.2.9-WIN distro

 - karaf4uAAL2.2.9-Linux and karaf4uAAL2.2.9-WIN: Are the basic karaf distro that contains the uAAL platform but they require the manual installation of it, by installing the proper features.

NOTE
----
For all the content that is not listed in this file, we can assume that the content is either experimental or obsolete
