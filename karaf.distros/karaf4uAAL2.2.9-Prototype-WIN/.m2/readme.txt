The content of this directory was removed because it contained large amounts of binary files. Those files should be distributed and stored in a different way, e.g. as a downloadable file in gforge or via ftp.

If you still need these files, check out an older version.